/*
	@author		Kristoffer Hell
	@version 	2021-06-08
*/

package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

type Person struct {
	id          string
	name        string
	dob         string
	address     string
	description string
	createdat   string
	updatedat   string
}

func main() {

	initDB()

	r := gin.Default()

	// ROOT ENTRY
	r.LoadHTMLFiles("index.tmpl")
	r.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.tmpl", gin.H{
			"": "",
		})
	})

	// READ
	// test: curl http://localhost:8080/persons
	r.GET("/persons", func(c *gin.Context) {
		//		id := c.Param("id")
		s := getPersons()
		c.JSON(http.StatusOK, s)
	})

	// READ
	// test: curl http://localhost:8080/person/4
	r.GET("/person/:id", func(c *gin.Context) {
		id := c.Param("id")
		p := getPerson(id)
		c.JSON(http.StatusOK, asJson(p))
	})

	// CREATE
	// test: curl -X POST -d "name=John Doe III&dob=1997-09-20&address=97th Street&description=A Jolly Good Fellow" http://localhost:8080/person
	r.POST("/person", func(c *gin.Context) {
		var p Person
		p.name = c.PostForm("name")
		p.dob = c.PostForm("dob")
		p.address = c.PostForm("address")
		p.description = c.PostForm("description")
		id := createPerson(p)
		fmt.Println("id=" + id)
		c.JSON(http.StatusOK, id)
	})

	// DELETE
	// test: curl -X DELETE "http://localhost:8080/person/14"
	r.DELETE("/person/:id", func(c *gin.Context) {
		id := c.Param("id")
		deletePerson(id)
		c.JSON(http.StatusOK, "deleted person with record id="+id)
	})

	// UPDATE
	// test: curl -X PUT -d 'dob=1977-01-12' http://localhost:8080/person/15
	r.PUT("/person/:id", func(c *gin.Context) {
		var p Person
		p.id = c.Param("id")
		p.name = c.PostForm("name")
		p.dob = c.PostForm("dob")
		p.address = c.PostForm("address")
		p.description = c.PostForm("description")
		updatePerson(p)
		c.JSON(http.StatusOK, "record with id "+p.id+" updated")
	})

	r.Run(":8080")
}
