module github.com/my/repo

go 1.16

require (
	github.com/alexbrainman/sspi v0.0.0-20180613141037-e580b900e9f5 // indirect
	github.com/gin-gonic/gin v1.6.3 // indirect
	github.com/go-redis/redis/v8 v8.6.0 // indirect
	github.com/jcmturner/gokrb5/v8 v8.2.0 // indirect
	github.com/lib/pq v1.10.2 // indirect
	github.com/shopspring/decimal v1.2.0 // indirect
	github.com/sirupsen/logrus v1.8.0 // indirect
	gopkg.in/jcmturner/aescts.v1 v1.0.1 // indirect
	gopkg.in/jcmturner/dnsutils.v1 v1.0.1 // indirect
	gopkg.in/jcmturner/goidentity.v3 v3.0.0 // indirect
	gopkg.in/jcmturner/gokrb5.v7 v7.5.0 // indirect
	gopkg.in/jcmturner/rpc.v1 v1.1.0 // indirect
	gorm.io/driver/mysql v1.0.4 // indirect
	gorm.io/driver/postgres v1.1.0 // indirect
)
