# golang test: Kristoffer Hell, June 2021

## prerequisites

`go`, `gin`, `postgresql` installed

## database table

```
-- Table: public.royce_test
-- DROP TABLE public.royce_test;

CREATE TABLE IF NOT EXISTS public.royce_test
(
    id integer NOT NULL DEFAULT nextval('royce_test_id_seq'::regclass),
    name text COLLATE pg_catalog."C.UTF-8",
    dob date,
    address text COLLATE pg_catalog."default",
    description text COLLATE pg_catalog."default",
    createdat timestamp without time zone,
    updatedat timestamp without time zone,
    CONSTRAINT royce_test_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public.royce_test
    OWNER to postgres;
```

## create some test data

```
insert into royce_test(name,dob,address,description,createdat,updatedat) values ('Alpha','1900-01-01','A street','A desc',now(),now());

insert into royce_test (name,dob,address,description,createdat,updatedat) values ('Bravo','1901-01-01','B street','B desc',now(),now());

insert into royce_test (name,dob,address,description,createdat,updatedat) values ('Charlie','1902-01-01','C street','C desc',now(),now());

insert into royce_test (name,dob,address,description,createdat,updatedat) values ('Delta','1903-01-01','D street','D desc',now(),now());

insert into royce_test (name,dob,address,description,createdat,updatedat) values ('Echo','1904-01-01','E street','E desc',now(),now());

insert into royce_test (name,dob,address,description,createdat,updatedat) values ('Foxtrot','1905-01-01','F street','F desc',now(),now());

insert into royce_test (name,dob,address,description,createdat,updatedat) values ('Golf','1906-01-01','G street','G desc',now(),now());
```
## run

run `./go.sh` from CLI

[if not on linux]

```
go run main.go dbsql.go
```

## demo/test cURLs

Take the browser to `localhost:8080`

cURLs for demo / testing are listed there...

