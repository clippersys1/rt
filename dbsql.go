/*
	@author		Kristoffer Hell
	@version 	2021-06-09
*/

package main

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"strings"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "royce2021"
	dbname   = "postgres"
)

var conn *sql.DB

func initDB() {
	conn, _ = getConn()
}

func closeDB() {
	defer conn.Close()
}

func getConn() (*sql.DB, error) {
	psqc := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)
	db, err := sql.Open("postgres", psqc)
	if err != nil {
		panic(err)
	}
	return db, nil
}

func updatePerson(p Person) {
	p2 := getPerson(p.id)
	if p.name != "" {
		p2.name = p.name
	}
	if p.dob != "" {
		p2.dob = p.dob
	}
	if p.address != "" {
		p2.address = p.address
	}
	if p.description != "" {
		p2.description = p.description
	}
	query := fmt.Sprintf("update royce_test set name='%s', dob='%s', address='%s', description='%s', updatedat=now() where id=%s;", p2.name, p2.dob, p2.address, p2.description, p.id)
	fmt.Println(query)
	err := conn.QueryRow(query)
	if err != nil {
		fmt.Println("went South")
	}
}

func deletePerson(id string) {
	query := "delete from royce_test where id = " + id
	fmt.Println("QUERY: " + query)
	err := conn.QueryRow(query)
	if err != nil {
		fmt.Println("went South")
	}
}

func createPerson(p Person) string {
	query := "insert into royce_test (name, dob, address, description,createdat,updatedat) values('"
	query += p.name + "','" + p.dob + "','" + p.address + "','" + p.description + "',now(),now());"
	fmt.Println(query)
	err := conn.QueryRow(query)
	if err != nil {
		fmt.Println("went South")
	}
	id := getLatestId(p.name, p.dob)
	return id
}

func getLatestId(n string, d string) string {
	var id string
	query := "select id from royce_test order by id desc limit 1;"
	row := conn.QueryRow(query)
	switch err := row.Scan(&id); err {
	case sql.ErrNoRows:
		fmt.Println("no such id: " + id)
	case nil:
		// a winner
		fmt.Println(id)
	default:
		panic(err)
	}
	return "{id:" + id + "}"
}

func getPersons() string {
	var sb strings.Builder
	var id string
	sb.WriteString("{")
	rows, err := conn.Query("select id from royce_test")
	if err != nil {
		fmt.Println(err)
	}
	for rows.Next() {
		err := rows.Scan(&id)
		if err != nil {
			fmt.Println(err)
		}
		sb.WriteString("id:\"" + id + "\"")
	}
	err = rows.Err()
	if err != nil {
		fmt.Println(err)
	}
	sb.WriteString("}")
	return sb.String()
}

func getPerson(id string) Person {
	var p Person
	p.id = id
	query := "SELECT name, dob, address, description, createdat, updatedat FROM royce_test WHERE id=" + id + ";"
	row := conn.QueryRow(query)
	switch err := row.Scan(&p.name, &p.dob, &p.address, &p.description, &p.createdat, &p.updatedat); err {
	case sql.ErrNoRows:
		fmt.Println("no such id: " + id)
	case nil:
		// a winner
		fmt.Println(p.name)
	default:
		panic(err)
	}
	return p
}

func asJson(p Person) string {
	s := fmt.Sprintf("{id:%s,name:%q,dob:%q,address:%q,description:%q,createdat:%q,updatedat:%q}", p.id, p.name, p.dob, p.address, p.description, p.createdat, p.updatedat)
	return s
}
